## Django Models
***

* Model in Django is an object that is saved in the database. 
* A database is a place in which a user’s information is stored.
* We can think of a model in the database as a spreadsheet with columns (fields) and rows (data).
* Generally, each model represents a single database table.


* Django model creates a corresponding table in the database

![IMAGE_DESCRIPTION](https://techvidvan.com/tutorials/wp-content/uploads/sites/2/2021/07/Django-model-normal-image01.jpg)

* Foreign key links in Django models create relationships between tables.
* This is how the syntax of models.py file appears.
* This example model defines a Person, which has a first_name and last_name:

```
from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
```

