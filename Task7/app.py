import requests
import datetime 

# Chirpstack - credentials & urls
chirpstack_email = "arunvk@icfoss.org"  # Replace with your Chirpstack email
chirpstack_password = "password@123"  # Replace with your Chirpstack password
chirpstack_url = "https://lorawandev.icfoss.org"  # Replace with your Chirpstack server URL

# Function to get device information
# API - /api/applications
def get_applications_info(url, token): 
    # Set headers with authentication token
    headers = {
        "Accept": "application/json",
        "Grpc-Metadata-Authorization": "Bearer " + token
    }
    # Make a GET request to fetch applications information
    response = requests.get(url + "/api/applications?limit=10", headers=headers, stream=False)
    print(response)
    #Parse the JSON response and extract device details
    response = response.json()
    response = [app['name']for app in response['result']]
    return response

# Function to get JWT token for authentication
# API - /api/internal/login
def apilogin(email, password, url):
    url = url + '/api/internal/login'
    credentials = '{"password": "' + password + '","email": "' + email + '"}'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    # Make a POST request to obtain the JWT token
    response = requests.post(url, data=credentials, headers=headers)
    data = response.json()
    print(data)
    return data['jwt']

# Main function
def main():
    # Get the JWT token for authentication
    token = apilogin(chirpstack_email, chirpstack_password, chirpstack_url)
    
    # Fetch and print device information
    applications_info = get_applications_info(chirpstack_url, token)
    print(applications_info)
    
    # Fetch and print application names
    response = get_applications_info(chirpstack_url,token)
    
    for name in response:
        print(name)
           
     
if __name__ == '__main__':
  
    main()
