# Signals and Custom management commands
***

* Signals and custom management commands are powerful tools that allow you to perform certain actions at specific points in the execution of your application and automate common tasks.

### Signals
***
* Django signals allow decoupled applications to get notified when certain actions occur elsewhere in the application. 

* They are a form of communication between different parts of a Django application without creating strong dependencies between them.


1. Define a Signal
***

* Create a signals.py file in one of your apps, and define a signal.

```
# signals.py
from django.db.models.signals import Signal
from django.dispatch import receiver

my_signal = Signal()

```

2. Connect signal to a function
***

* Connect the signal to a function using the @receiver decorator.

```
# signals.py
from django.db.models.signals import Signal
from django.dispatch import receiver

my_signal = Signal()

@receiver(my_signal)
def my_signal_handler(sender, **kwargs):
    print("Signal received!")
```

3. Connect signal in Appconfig
***

```
# apps.py
from django.apps import AppConfig

class MyAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'myapp'

    def ready(self):
        import myapp.signals
```

4. Send signal in code
***

```
# somefile.py
from myapp.signals import my_signal

my_signal.send(sender=None)
```


### Custom Management command
***

1. Create a management command

* Create a folder named management/commands within your app, and then create a Python file for your command.

```
# myapp/management/commands/my_command.py
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'My custom management command'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Successfully ran my custom command'))
```

2. Run the management command

```
python manage.py my_command

```

# Custom Template Tags and Filters
***


### Custom Template tags

* custom template tags and filters allow you to extend the functionality of your templates by creating custom template tags or filters that perform specific tasks. 

1. Create a 'templatetags' directory
***

```
myapp/
  ├── __init__.py
  ├── models.py
  ├── templatetags/
  │    └── __init__.py
  └── ...

```

2. Create a python file for your tag
***

* Inside the templatetags folder, create a Python file (e.g., custom_tags.py).

```

# myapp/templatetags/custom_tags.py
from django import template

register = template.Library()

@register.simple_tag
def my_custom_tag(parameter):
    # Your logic here
    return f"Processed parameter: {parameter}"

```

3. Load and Use the Custom Tag in Templates

```
{% load custom_tags %}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Template</title>
</head>
<body>
    <p>{% my_custom_tag "Hello" %}</p>
</body>
</html>

```

### Custom template filters
***

* Custom template filters allow you to process variables in your templates, modifying their output or formatting.

1. Create a templatetags Directory (if not created already):

2. Create a Python File for Your Filter
***

* Inside the templatetags folder, create a Python file (e.g., custom_filters.py).

```
# myapp/templatetags/custom_filters.py
from django import template

register = template.Library()

@register.filter
def my_custom_filter(value):
    # Your logic here
    return f"Filtered value: {value}"

```

3. Load and Use the Custom Filter in Templates

```

{% load custom_filters %}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Template</title>
</head>
<body>
    <p>{{ some_variable|my_custom_filter }}</p>
</body>
</html>

```

### Register the templatetags folder
***

* In your app's __init__.py file, add the following:

```

# myapp/templatetags/__init__.py
from django import template

register = template.Library()

# Import your custom tags and filters
from . import custom_tags
from . import custom_filters

```


# Extending django with third-party packages
***

* Extending Django with third-party packages is a common practice that allows you to leverage existing solutions and add functionalities to your Django projects.

1. Install third-party packages

```
pip install package-name 

```
2. Add Installed Packages to INSTALLED_APPS

* In your project's settings.py, add the installed packages to the INSTALLED_APPS setting.

```
# settings.py

INSTALLED_APPS = [
    # ...
    'package_name',
    # ...
]

```

3. Run migrations

* If the package includes database models, run migrations to apply changes to the database.

```
python manage.py makemigrations
python manage.py migrate

```

### Example of popular third party package:
***

* Django REST framework
    * Popular for building APIs for your django applications.

```
    pip install djangorestframework

```

