from flask import Flask, render_template

import requests
import datetime 

app = Flask(__name__)

chirpstack_email = ""  # Replace with your Chirpstack email
chirpstack_password = ""  # Replace with your Chirpstack password
chirpstack_url = ""  # Replace with your Chirpstack server URL

@app.route('/')
def index():

    data =apilogin('','','') #Replace with email,password and url
    print(data)
    data = get_gateways_info()
    print(data)
    gateway_data=data
    return render_template('index.html',data={"data":data,"gw":gateway_data})

# Function to get device information
# API - /api/applications

@app.route('/api/chirpstack-data-applications')
def get_applications_info(): 
    
    url = ""
    token = ""
    

    # Set headers with authentication token
    headers = {
        "Accept": "application/json",
        "Grpc-Metadata-Authorization": "Bearer " + token
    }
    # Make a GET request to fetch applications information
    response = requests.get(url + "/api/applications?limit=5", headers=headers, stream=False)
    print(response)
    #Parse the JSON response and extract device details
    data = response.json()
    response = [app['name']for app in data['result']]   
    return response

# API - api/gateways
def get_gateways_info():
    url = ""
    token = ""

    #Set headers with authentication token
    headers = {
        "Accept": "application/json",
        "Grpc-Metadata-Authorization": "Bearer " + token
    }
    
    #Make a GET request to fetch gateways information
    response = requests.get(url + "/api/gateways?limit=10", headers = headers, stream=False)
    print(response)
    data = response.json()
    print(data)
    
    gateway_data = {}
  
    for item in data['result']:
        gateway_name = item['name']
        lat = item.get('location',{}).get('latitude')
        lon = item.get('location',{}).get('longitude')
        
        gateway_data[gateway_name] = {
            'latitude' : lat,
            'longitude' : lon,            
        }
    print(gateway_data)
    return gateway_data    
        

def apilogin(email, password, url):

    url = url + '/api/internal/login'
    credentials = '{"password": "' + password + '","email": "' + email + '"}'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    # Make a POST request to obtain the JWT token
    response2 = requests.post(url, data=credentials, headers=headers)
    data = response2.json()
    print(data)
    return data['jwt']

# Main function
def main():
      
    # Get the JWT token for authentication
    token = apilogin(chirpstack_email, chirpstack_password, chirpstack_url)
    
    # Fetch and print device information
    applications_info = get_applications_info(chirpstack_url, token)
    print(applications_info)
    
    # Fetch and print application names
    response = get_applications_info(chirpstack_url,token)
    
    #Fetch and print gateway information
    gateway_info = get_gateways_info(chirpstack_url,token)
    print(gateway_info)
    
    #Fetch and print gateway latitudes and longitudes
    response2 = get_gateways_info(chirpstack_url,token)
    
    for name in response:
        print(name)   
        return render_template('index.html',data=name)
           
    for latitude in response2:
        print(latitude)
        return render_template('index.html',data=latitude)
     

if __name__ == '__main__':
    app.run(debug=True)
