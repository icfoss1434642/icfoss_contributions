# Chirpstack Software
***

* open-source LoRaWAN network server stack. 
* provide components for building LoRaWAN networks.
* LoRaWAN(Long Range Wide Area Network) is a wireless communincation technology that enables long-range communincation between low range devices.

### Components
***

1. Chirpstack Gateway Bridge:
    * Responsible for translating the proprietary gateway protocol into standard MQTT protocol.
    * Recieves data from LoRa gateways --> collect information --> publishes data to MQTT

2. Message Broker(eg:MQTT):
    * Chirpstack uses a message broker to handle communication between different components.
    * Responsible for distributing messages from gateway bridge to appropriate components.

3. Chirpstack Network Server:
    * Network server is a core component responsible for managing the communication between the devices and the application server.
    * decodes raw messages from gateways --> validates them --> forwards to application server
    * Also handles downlink messages and routes them to correct gateways

4. Chirpstack Application Server:
    * Responsible for managing the application logic and processing data from end devices.
    * Supports various integration options such as HTTP, MQTT, etc.


### Workflow:
***

*  Device Activation:
    * A LoRaWAN device is activated by sending an activation message.
    * Network server validates the activation request and assigns a network session key to the device.

*  Data transmission:
    * The LoRaWAN device transmits data at scheduled intervals.
    * The data is received by nearest gateway, which forwards it to the Chirpstack gateway bridge.

*  Data Processing:
    * The gateway bridge translates the data and sent it to Chirpstack Network Server.
    * The network server process the data, validates it and forwards it to the chirpstack application server.

*  Application Processing:
    * The application Server process the data, applies business logic and forwards the relevant information to user applications.


## MQTT Communication Protocol
***

* MQTT (Message Queuing Telemetry Transport)
* Used as a communication protocol between different components of the LoRaWAN network.
* Light-weight and efficient.

1. MQTT broker
***
* An intermediary server that handles the distribution of messages between publishers and subsribers.
* Here it routes messages from Chirpstack Gateway Bridge to other Chirpstack Components.
* Various MQTT brokers are Mosquitto, RabbitMQ, etc.













