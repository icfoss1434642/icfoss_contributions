### InfluxDB Installation
***

* Download InfluxDB using the following command

   $ wget https://dl.influxdata.com/influxdb/releases/influxdb_1.8.10_amd64.deb

* Installing the InfluxDB package

   $ sudo dpkg -i influxdb_1.8.10_amd64.deb

* Add InfluxData repository with the following command

   $ # influxdata-archive_compat.key GPG Fingerprint: 9D539D90D3328DC7D6C8D3B9D8FF8E1F7DF8B07E

   $ wget -q https://repos.influxdata.com/influxdata-archive_compat.key

   $ echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null

   $ echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

* Install and start the service 

   $ sudo apt-get update && sudo apt-get install influxdb

   $ sudo service influxdb start

* Download and import  InfluxData public key

   $ curl -s https://repos.influxdata.com/influxdata-archive_compat.key | gpg --import

* Download the signature file for the release by adding .asc to the download url

   $ wget https://dl.influxdata.com/influxdb/releases/influxdb-1.8.10_linux_amd64.tar.gz.asc

* Verify the signature withgpg --verify

   $ gpg --verify influxdb-1.8.10_linux_amd64.tar.gz.asc influxdb-1.8.10_linux_amd64.tar.gz

* Point the process to the correct configuration file by using the -config option

   $ influxd -config /etc/influxdb/influxdb.conf
