from influxdb import InfluxDBClient

host = 'localhost'
port = 8086
username = ''
password = ''
database = 'valorant'

client = InfluxDBClient(host, port, username, password, database)

query = 'SELECT * FROM stock'
result = client.query(query)

for point in result.get_points():
    print(f"Time: {point['time']}")
    for field_name, field_value in point.items():
        if field_name != 'time':
            print(f"{field_name}: {field_value}")

client.close()
