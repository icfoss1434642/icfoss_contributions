# Git
***

1. Initialise an empty repo

```
git init 

```

2. List all the versions (commits)

```
git log --all

```

3. go to that specific version

```
 git checkout version_address
```

4. To create a new branch

```
git branch branch_name

```
5. Go to a specific branch

```
git checkout branch_name

```
