## Creating a Django project and app
***

* Django is a python framework. So python sholud be downloaded and installed.

* Install a virtual environment

```
    pip install virtualenv

```

* Create a virtual environment

```
    python3 -m venv myenv

```

* activate the virtual environment

```
    source myenv/bin/activate

```

* Install django

```
    pip install django

```

* Create your django project

```
    django-admin startproject myproject

```

* Navigate to project directory

```
    cd myproject

```

* Create a django app

```
    python3 manage.py startapp myapp

```

* Database setup and to make migrations

```
    python3 manage.py makemigrations
    python3 manage.py migrate

```

* To run the the application

```
    python3 manage.py runserver

```

* To create an admin

```
    python3 manage.py createsuperuser

```


## Understanding the project structure
***

![IMAGE_DESCRIPTION](https://techvidvan.com/tutorials/wp-content/uploads/sites/2/2021/06/Django-Project-STructure_3.png)


1. __init__.py
***

* This is an empty file as you can see in the image.
* The function of this file is to tell the Python interpreter that this directory is a package and involvement of this __init.py_ file in it makes it a python project.

2. settings.py
***

* It contains the Django project configuration.
* It is used for adding all the applications and middleware applications.
* This is the main setting file of the Django project. 
* It contains sqlite3 as the default database. We can change this database to Mysql, PostgreSQL, or MongoDB according to the web application we create.
* It contains some pre-installed apps and middleware that are there to provide basic functionality.

3. urls.py
***

* URL is a universal resource locator.
* It contains all the endpoints that we should have for our website.
* It is used to provide you the address of the resources (images, webpages, websites, etc) that are present out there on the internet.
* In simpler words, this file tells Django that if a user comes with this URL, direct them to that particular website or image whatsoever it is.

4. wsgi.py
***

* When you will complete your journey from development to production, the next task is hosting your application.
* Here you will not be using the Django web server, but the WSGI server will take care of it.
* WSGI stands for Web Server Gateway Interface,
* It describes the way how servers interact with the applications.
* It is a very easy task, you just have to import middleware according to the server you want to use.
* For every server, there is Django middleware available that solves all the integration and connectivity issues.

5. asgi.py
***

* ASGI works similar to WSGI but comes with some additional functionality.
* ASGI stands for Asynchronous Server Gateway Interface.
* It is now replacing its predecessor WSGI.

## Django Application files
***

![IMAGE_DESCRIPTION](https://techvidvan.com/tutorials/wp-content/uploads/sites/2/2021/06/Django_Project_Layout_Last.png)

1. __init__.py
***

* This file provides the same functionality as that in the _init_.py file in the Django project structure.
* It is an empty file and does not need any modifications.
* It just represents that the app directory is a package.

2. admin.py
***

* Admin.py file is used for registering the Django models into the Django administration.
* It performs three major tasks:
    * Registering models
    * Creating a Superuser
    * Logging in and using the web application

3. apps.py
***

* apps.py is a file that is used to help the user include the application configuration for their app.
* Users can configure the attributes of their application using the apps.py file.
* However, configuring the attributes is a rare task a user ever performs, because most of the time the default configuration is sufficient enough to work with.

4. models.py
***

* Models.py represents the models of web applications in the form of classes.
* It is considered the most important aspect of the App file structure.
* Models define the structure of the database.
* It tells about the actual design, relationships between the data sets, and their attribute constraints. 

5. views.py
***

* Views provide an interface through which a user interacts with a Django web application.
* It contains all the views in the form of classes.
* We use the concept of Serializers in Django Rest_Framework for making different types of views.
* Some of these are CustomFilter Views, Class-Based List Views, and Detail Views.

6. urls.py
***

* Urls.py works the same as that of the urls.py in the project file structure.
* The primary aim being, linking the user’s URL request to the corresponding pages it is pointing to.
* You won’t find this under the app files. We create this by clicking on the New file option.

7. tests.py
***

* Tests.py allows the user to write test code for their web applications.
* It is used to test the working of the app.


manage.py
***

* manage.py is also a command-line utility for Django projects.
* When you use the startproject command, each Django project includes a Python script called manage.py file that is generated automatically.
* It is placed in the root directory of the current project.


## Intoduction to django models, views and templates
***

### Model
***

* The model provides data from the database.
* In Django, the data is delivered as an Object Relational Mapping (ORM), which is a technique designed to make it easier to work with databases.
* The most common way to extract data from a database is SQL.
* One problem with SQL is that you have to have a pretty good understanding of the database structure to be able to work with it.
* Django, with ORM, makes it easier to communicate with the database, without having to write complex SQL statements.
* The models are usually located in a file called models.py.

### View
***

* A view is a function or method that takes http requests as arguments, imports the relevant model(s), and finds out what data to send to the template, and returns the final result.
* The views are usually located in a file called views.py.

### Template
***

* A template is a file where you describe how the result should be represented.
* Templates are often .html files, with HTML code describing the layout of a web page, but it can also be in other file formats to present other results, but we will concentrate on .html files.
* Django uses standard HTML to describe the layout, but uses Django tags to add logic:

```
<h1>My Homepage</h1>

<p>My name is {{ firstname }}.</p>

```
* The templates of an application is located in a folder named templates.











































