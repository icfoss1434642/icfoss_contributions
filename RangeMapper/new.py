
##This program fetch data from mqtt broker and displays it on influxdb2.0

from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point
import paho.mqtt.client as mqtt
import json

url = "http://localhost:8086"
token = "tCNu_8aABDBrBnHqFcTUzqdY4IJSEsxIGx_6kumpj5hlllD7g3wbp4CBjZ0YT_THnFWfJR-DrTT4rly0tX_6zA=="
org = "NewOrg"
bucket = "MQTT"

# MQTT credentials
mqtt_username = ""
mqtt_password = ""
mqtt_address = ""
mqtt_port = 1883
mqtt_client_id = ""

# Create an InfluxDB client
client = InfluxDBClient(url=url, token=token, org=org)

# Create a write API instance
write_api = client.write_api(write_options=SYNCHRONOUS)

# Topic for displaying everything related to a specific APPLICATION_ID (eg:39)
application_individual_topic = "application/39/#"

def on_connect(client, userdata, flags, rc):
    """
    Callback function called when the client connects to the broker.
    """
    print("Connected with result code " + str(rc))

    # Subscribe to the desired MQTT topics
    client.subscribe(application_individual_topic)

def on_message(client, userdata, msg):
    """
    Callback function called when a message is received.
    """
    try:
        # Decode the payload as JSON
        payload = json.loads(msg.payload.decode())
        print(payload)

        # Extract relevant data from the payload
        device_name = payload.get('deviceName')
        device_id = payload.get("devEUI")
        payload_fields = payload.get("object", {})

        # Prepare data for InfluxDB
        point = Point("MQTT2") \
            .tag("device_id", device_id) \
            .tag("device_name", device_name)\
       
        for field, value in payload_fields.items():
            print(field,end=" : ")
            print(value)
            
            try:
                value = float(value)  # Convert "Temperature" to float
                point.field(field, value)
            except (ValueError, TypeError):
                print(f"Skipping non-numeric value for field : {field}")

        # Send data to InfluxDB
        write_api.write(bucket=bucket, org=org, record=point)

    except json.JSONDecodeError as e:
        print("Error decoding JSON:", str(e))
        return

# Create an MQTT client instance
mqtt_client = mqtt.Client(mqtt_client_id)

# Set the username and password for authentication
mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)

# Connect to the MQTT broker
mqtt_client.connect(mqtt_address, mqtt_port, 60)

# Set callback functions
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message

# Start the MQTT client loop (blocking)
mqtt_client.loop_forever()
