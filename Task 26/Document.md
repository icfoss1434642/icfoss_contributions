# Django - Static and media files
***

* In Django, static files and media files serve different purposes.

### Static files
***

* Static files are files that don't change during the runtime of a Django application. 
* Examples include CSS files, JavaScript files, images, etc.
* Django provides a built-in way to handle static files using the STATICFILES_DIRS and static template tag.

1. Settings configuration
***

* In your settings.py file, you need to define STATIC_URL and optionally STATICFILES_DIRS if you have static files in locations other than the app's static folder.

```
# settings.py

STATIC_URL = '/static/'
# STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

```

2. Usage in templates
***

* In your templates, you can use the {% load static %} template tag and reference static files like this:

```
{% load static %}

<link rel="stylesheet" type="text/css" href="{% static 'css/style.css' %}">

```

3. Collecting static files
***

* During deployment, you typically run collectstatic to gather all static files into a single directory. This is then served by your web server.

```
python manage.py collectstatic

```

### Media files
***

* Media files are user-uploaded files, such as images or documents.
* Django provides a built-in way to handle media files during development
*  But in production, it's common to serve media files using a dedicated web server like Nginx or Apache, or using cloud storage services.


1. Settings Configuration
***

* In your settings.py file, you need to define MEDIA_URL and MEDIA_ROOT:

```
# settings.py

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

```

* MEDIA_URL is the base URL to serve media files, and MEDIA_ROOT is the local filesystem path where these files will be stored.

2. URL Configuration
***

* In your urls.py, add a URL pattern to serve media files during development:

```
# urls.py
from django.conf import settings
from django.conf.urls.static import static

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

```

*  This should only be used during development. In production, you should configure your web server to serve media files.

3. Usage in Models
***

* In your models, use FileField or ImageField to handle media file uploads:

```
# models.py
from django.db import models

class MyModel(models.Model):
    my_file = models.FileField(upload_to='uploads/')

```

4. File Upload form
***

* When creating a form for file uploads, remember to set the enctype attribute to 'multipart/form-data'.

```
<form method="post" enctype="multipart/form-data">
    <!-- Your form fields here, including the file input -->
</form>

```
