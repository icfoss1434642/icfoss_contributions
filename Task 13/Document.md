# Python Functions
***

* Functions are block of reusable code that performs a specific task
* They are defined using def keyword

    def function_name(parameter1,parameter2):<br>
    #Function body<br>
    #Performs some tasks using parameters<br>
        return result

* Given below is an example to call and define a Function

    def greet(name): <br>
    print(f"Hello, {name}.....")

calling the function:

    greet(Alice)

Result:

    Hello Alice.....

### Types of Functions
***

1. Built-in functions:
***
* These are functions that are available in python's standard library and can be used without importing any additional modules. Examples include 'print()', 'len()', 'input()'. 'range()', 'type()', 'str()'.

2. User-defined functions:
***
* These are functions created by the user to perform a particular task.
* we define them using 'def' keyword

3. Recursive functions:
***
* These are functions that calls themselves to solve a problem.
* These are useful for problems that can be broken down into smaller,similar subproblems.

    def factorial(n):<br>
    if n==0: <br>
    return 1 <br>
    else: <br>
    return n*factorial(n-1)
