from django.test import TestCase
from blog.models import MyModel

# Create your tests here.
class MyModelTestCase(TestCase):
    def test_get_full_info(self):
        
        my_model = MyModel(name = "Test", description = "this is a test")
        
        full_info = my_model.get_full_info()
        
        expected_info = "Test: this is a test"
        
        self.assertEqual(full_info, expected_info)