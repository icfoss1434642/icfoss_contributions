# Task 1
*** 

### Start Date : 09/10/2023 , End Date : 09/10/2023

* Create a folder named "Flask_Learning".
* Create a simple flask application that has a simple homepage (just a welcome screen).
* Document the procedure and add the code and documentation inside the "Flask_Learning" folder.

# Task 2
***

### Start Date :17/10/2023 , End Date : 19/10/2023

Go through the given link and learn how to access the chirpstack API, fetch data, and print it on the console.

https://tekworldthink.blogspot.com/2023/09/how-to-access-chirpstack-api-python.html

# Task 3
***

### Start Date : 10/10/2023 , End Date : 10/10/2023 

Create a simple HTML page that integrates OpenStreetMap. Add a marker in Greenfield Stadium using javascript, take screenshots, and upload them to a folder named "Open Street Map.".

# Task 4
***

### Start Date : 16/10/2023 , End Date : 16/10/2023 

Create a flask application and show an open street map on the home page.


# Task 5
***

### Start Date : 19/10/2023 , End Date : 19/10/2023 

* What is API?
* Purpose?
* API endpoints?
* API Use case?
* API data formats.

# Task 6
***

### Start Date : 20/10/2023 , End Date : 20/10/2023 

* HTTP resoponse codes and meanings.
* Authentication vs Authorization

# Task 7
***

### Start Date : 20/10/2023 , End Date : 20/10/2023 

* Write the code to fetch and print only the names of all the applications from the Chirpstack server

# Task 8
***

### Start Date : 25/10/2023 , End Date : 25/10/2023 

* Make a flask application that shows data into an html page fetched from the server

# Task 9
***

### Start Date : 25/10/2023 , End Date : 27/10/2023

* Develop a Flask application that incorporates an OpenStreetMap, and enhance it by displaying three coordinates on the map, along with fetching application data from "lorawandev.icfoss.org" and presenting this data at each of the specified coordinates."


# Task 10
***

### Start Date : 28/10/2023 , End Date : 

* Fetch the location details from the '/api/gateways' and display corresponding markers on the map.

# Task 11
***

### Start Date: 28/10/2023, End Date :

Make a documentation of the following:
* Client-Server Architecture
* URL
* URL Structure
* URI
* URL vs URI
* HTTP endpoints

# Task 12
*** 

### Start Date :           , End Date :

* Display the applications names from '/api/applications' for each marker created from the previous task.

# Task 13
***

### Start Date :          , End Date :

* Explain Functions and its types in python

# Task 14
***

### Start Date:          , End Date :

* Install and document influxdb version 1x
* Write data into influxdb
* Fetch data from influxdb using Python script

# Task 15
***

* Write code to perform unit testing in django.
* Make a proper documentation.

# Task 16
***

* Introduction to web development and django
    * Understanding web development concepts.
    * Overview of Django and its role in web development.
    * Installation and setup of Django.

# Task 17
***

* Django Basics
    * Creating a Django project and app.
    * Understanding the project structure.
    * Introduction to Django models, views, and templates.

# Task 18
***

* Django models
    * Defining models and database tables.
    * Performing database migrations.
    * Querying the database using the Django ORM (Object-Relational Mapping).


# Task 19
***

* Django Views and templates
    * Creating views and templates.
    * Passing data from views to templates.
    * URL patterns and routing in Django.

# Task 20
***

* Django Forms 
    * Building HTML forms with Django.
    * Handling form submissions and validations.
    * Form processing in views

# Task 21
***

* Django Admin Interface
    * Exploring and customizing the Django admin interface.
    * Creating admin views for managing data.

# Task 22
***

* Study chirpstack software , its components and working.
* Install chirpsatck on local machine(V3)
* MQTT communication protocol

# Task 23
***

* Plot LoRaWAN applications on the map
* Each device from the selected applications should be plotted using the coordinates from the description of the application
* On clicking on the device marker , the last sensory data should be displayed including the devices last seen

# Task 24
***

* Django middleware
    * Understanding middlewares in django
    * Creating custom middleware components

# Task 25
***

* Django REST Framework
    * Introduction to Django REST Framework for building APIs
    * Creating serializers and views for API endpoints.
    * Consuming APIs in Django.

# Task 26
***

* Django Static Files and media
    * Managing static files (CSS, JavaScript) in Django.
    * Handling media files (uploads, storage) in Django.

# Task 27
***

* Django testing
    * Writing unit tests for Django applications
    * Running and interpreting test results.

# Task 28
***

* Django Deployment
    * Preparing a Django project for deployment.
    * Deploying a Django application to a web server (e.g., Heroku, AWS).

# Task 29
***

* Django Security best practices
    * Securing Django applications against common vulnerabilities.
    * Implementing secure authentication and authorization.

# Task 30
***

* Django Advanced Concepts
    * Signals and custom management commands.
    * Building custom template tags and filters.
    * Extending Django with third-party packages.

# Task 31
***

* User Authentication and Authorization
    * implementing user registration and login.
    * Managing user sessions and authentication.
    * Setting up user permissions and access control.
