## How to create a local git repository
***

* Open the terminal

* Navigate to the directory where you want to create your new repository

```
cd  /path/to/your/directory

```

* Initialise the new git repository

```
git init 

```
* Add your files to the staging area

```
git add .

```
* Commit your changes
***

```
git commit -m "Initial Commit"

```
* Clone a respository from a remote url
***

```
git clone <repository_url>

```

## How to create a github repository
***

* Login to github

* On the github homepage, click the "+" sign in the top right corner 

* Select new repository

* Fill the necessary information

* Click create repository

## How to push an existing local repo to github
***

* Create a repository on github
    * Login to your github account
    * On github homepage, click the "+" sign on right top corner
    * Select new repository
    * Select repository name and other optional details

* Navigate to your  Local repository

```
cd /path/to/your/local/repository

```

* Set up remote repository url

```
git remote add origin <repository_url>

```
* Push local repository to github

```
git push origin master

```
## How to contribute to a public repository in your account
***

* Fork the repository

* Clone your fork

```
git clone https://github.com/your-username/repository-name.git

```

* Create a branch

    * navigate to respository folder

```
    cd repository-name

```

* Create a new branch for your changes

```
    git branch branch_name

    git checkout branch_name

```

* Make changes

* Stage and commit changes

```
git add .

git commit -m "Your commit message"

```
* Push the changes to your fork

```
git push origin your-branch-name

```
* Create a Pull request
    * Visit your fork on github
    * You should see a button to create a pull request. Click it.
    * Provide a title and description for your pull request and then click "Create Pull Request".

* Await review and merge
    * The repository owners or maintainers will review your changes.
    * If everything is in order, they may merge your changes into the main repository.


* Keep your fork updated
    * If there are changes in the original repository that you forked, you should keep your fork updated:

    * Add the original repository as remote

```
git remote add upstream https://github.com/original-owner/repository-name.git

```
* Fetch changes from the original repository

```
git fetch upstream

```
* Merge changes into your local branch

```
git merge upstream/main

```
* Push the updates to your fork

```
git push origin your-branch-name

```













