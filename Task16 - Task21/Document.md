# Django Documentation
***

### Understanding web development concepts in django
***

##### Model-View-Controller (MVC) Architecture:

* Django follows the Model-View-Controller architectural pattern. In Django, this is often referred to as Model-View-Template (MVT).

* Models: Define the data structure of your application. Models are Python classes that define the fields and behavior of the data you are working with. Django automatically generates a database schema based on your models.

* Views: Handle the presentation logic. They receive data from the models and pass it to the templates for rendering.

* Templates: Define the structure of the output (HTML). Templates receive data from views and render it into a format that can be sent to the client.

##### URLs and Views:
***

* Django uses a URL dispatcher to map URLs to view functions. 

* URLs are defined in the urls.py file, and each URL pattern is associated with a specific view function.

* Views are Python functions or classes that handle the 
business logic for a specific URL pattern.

* They process requests and return responses.

##### Templates:
***

* Templates are used to generate HTML dynamically. 

* They can include variables, control structures, and template tags that enable you to loop through data or include other templates.

* Django uses its own template language that allows you to embed Python-like code directly in your HTML templates.

##### Forms:
***

* Django provides a powerful form handling system that simplifies the process of handling user input.

* Forms can be used to validate and process data submitted by users.

* Forms are created as Python classes, and Django takes care of rendering HTML forms, handling user input, and validating data.

##### Django ORM (Object-Relational Mapping):
***

* Django includes an ORM that allows you to interact with your database using Python code instead of SQL.

* Models define the structure of your database tables, and the ORM handles the translation between Python objects and database records.

##### Admin Interface:
***

* Django comes with a built-in admin interface that allows you to manage your application's data using a web-based UI.

* The admin interface is automatically generated based on your models.

##### Middleware:
***

* Middleware is a way to process requests globally before they reach the view or after the view has processed the request.

* Django comes with several built-in middleware components, and you can also write your own.

##### Static and Media Files:
***

* Django has support for managing static files (CSS, JavaScript, images) and media files (user uploads).

* The static and media apps and corresponding settings are used for this purpose.

##### Authentication and Authorization:
***

* Django provides built-in tools for user authentication and authorization.

* The django.contrib.auth module includes models and views for managing user accounts and permissions.

##### Django REST Framework:
***

* If you're building an API, Django REST Framework is a powerful and flexible toolkit for building Web APIs in Django. 

* It's an extension of Django that simplifies the process of building APIs.


### Installation and setup of Django
***

* Django is a python framework. So python sholud be downloaded and installed.

* Install a virtual environment

```
    pip install virtualenv

```

* Create a virtual environment

```
    python3 -m venv myenv

```

* activate the virtual environment

```
    source myenv/bin/activate

```

* Install django

```
    pip install django

```

* Create your django project

```
    django-admin startproject myproject

```

* Navigate to project directory

```
    cd myproject

```

* Create a django app

```
    python3 manage.py startapp myapp

```

* Database setup and to make migrations

```
    python3 manage.py makemigrations
    python3 manage.py migrate

```

* To run the the application

```
    python3 manage.py runserver

```

* To create an admin

```
    python3 manage.py createsuperuser

```


