## User registration and management
***

### User registration
***

* Just like creating an account for a new service, you need to register users.
* This involves setting up usernames and passwords.
* Each user might have different permissions, like some can only view data, while others can configure settings.


* Step 1: Navigate to the 'Users' tab on the left side of the screen and click to access.

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img1.png?ref_type=heads)


* Step 2: Select 'Add User' located in the upper-right corner

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img2.png?ref_type=heads)

* Step 3: Complete the necessary user information fields with accurate details.

* Is active : Customize this option based on your preferences; toggle it off to deny access to the website, or turn it on to grant access as desired.

* Is admin : Toggle it on or off to grant or restrict admin access and features accordingly.

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img3.png?ref_type=heads)

* This is how it looks like

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img4.png)

### User management
***

* After users are registered, you can manage them.
* This includes things like changing passwords, updating user roles (permissions), and, if needed, removing users who no longer need access.
