# Initial Setup and Configuration
***

## System Requirements
***

* Before you begin, make sure your computer or server meets certain requirements.
* This could include having a specific operating system (like Linux), enough memory, and other software prerequisites.

# Installation
***

* Install Mosquitto, Redis, PostgreSQL

```
sudo apt install mosquitto mosquitto-clients redis-server redis-tools postgresql
```
* Switch to the PostgreSQL command-line tool

```
sudo -u postgres psql
```
* Create a role for authentication

```
create role chirpstack with login password 'chirpstack';
```

* Create a database and set chirpstack as the owner

```
create database chirpstack with owner chirpstack;
```

* Switch to the chirpstack database

```
\c chirpstack
```

* Create the pg_trgm extension

```
create extension pg_trgm;
```

* Exit the PostgreSQL command-line tool

```
\q
```

* Install required packages for ChirpStack Gateway Bridge

```
sudo apt install apt-transport-https dirmngr
```

* Add the ChirpStack repository key

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1CE2AFD36DBCCA00
```

* Add the ChirpStack repository to the sources list

```
sudo echo "deb https://artifacts.chirpstack.io/packages/4.x/deb stable main" | sudo tee /etc/apt/sources.list.d/chirpstack.list
```

* Update package information

```
sudo apt Update
```

* Install ChirpStack Gateway Bridge

```
sudo apt install chirpstack-gateway-bridge
```


## Configuration
***

* Once installed, ChirpStack needs to know some basic things to work correctly. 
* Configure things like how devices connect, where data is stored, and how security is handled.


* Configure the ChirpStack Gateway Bridge by updating the MQTT integration section in the configuration file located at /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml

```
Example for EU868:
[integration.mqtt]
event_topic_template="eu868/gateway/{{ .GatewayID }}/event/{{ .EventType }}"
command_topic_template="eu868/gateway/{{ .GatewayID }}/command/#"
```
 Here EU868 is the frequency plan of Europe. Change the plan according to your country:
 You can refer frequency plans of all the countries from the link mentioned below:
 "https://www.thethingsnetwork.org/docs/lorawan/frequencies-by-country/"


* Start ChirpStack Gateway Bridge

```
sudo systemctl start chirpstack-gateway-bridge
```

* Start ChirpStack Gateway Bridge on boot

```
sudo systemctl enable chirpstack-gateway-bridge
```

* Install ChirpStack

```
sudo apt install chirpstack
```

* Configuration files for ChirpStack are located at /etc/chirpstack.

* Start ChirpStack

```
sudo systemctl start chirpstack
```

* Start ChirpStack on boot

```
sudo systemctl enable chirpstack
```

* Check ChirpStack logs if any error occurs while running the server.

```
sudo journalctl -f -n 100 -u chirpstack
```

* Access ChirpStack web interface by going to localhost:8080 in your browser
* The default login is admin as user, and the password is also admin.

## User registration and management
***

### User registration
***

* Just like creating an account for a new service, you need to register users.
* This involves setting up usernames and passwords.
* Each user might have different permissions, like some can only view data, while others can configure settings.


* Step 1: Navigate to the 'Users' tab on the left side of the screen and click to access.

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img1.png?ref_type=heads)


* Step 2: Select 'Add User' located in the upper-right corner

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img2.png?ref_type=heads)

* Step 3: Complete the necessary user information fields with accurate details.

* Is active : Customize this option based on your preferences; toggle it off to deny access to the website, or turn it on to grant access as desired.

* Is admin : Toggle it on or off to grant or restrict admin access and features accordingly.

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img3.png?ref_type=heads)

* This is how it looks like

![alt text](https://gitlab.com/icfoss1434642/icfoss_contributions/-/raw/main/images/img4.png)

### User management
***

* After users are registered, you can manage them.
* This includes things like changing passwords, updating user roles (permissions), and, if needed, removing users who no longer need access.

# Overview of ChirpStack Components
***

1. ChirpStack Network Server
***
* Purpose:
    * Acts as a traffic coordinator for LoRaWAN devices.
    * Manages communication between the IoT devices (sensors, trackers) and the application servers.
    * Ensures messages are properly delivered to the intended destination.

* Functionality:
    * Listens to messages from LoRaWAN devices.
    * Decodes and validates messages.
    * Routes messages to the appropriate destination within the network.

2. ChirpStack Application Server 
***
* Purpose:
    * Handles the data received from LoRaWAN devices.
    * Provides a dashboard for monitoring and analyzing device data.
    * Executes rules and actions based on received data.
* Functionality:
    * Collects and stores data from devices.
    * Allows users to set up rules for data processing.
    * Interfaces with external systems or applications for further actions.

3. ChirpStack Gateway Bridge
***
* Purpose:
    * Acts as a bridge between LoRaWAN devices and the ChirpStack Network Server.
    * Translates and forwards messages from devices to the Network Server.
* Functionality:
    * Listens to messages from LoRaWAN gateways.
    * Decodes and forwards messages to the ChirpStack Network Server.
    * Manages the communication protocol between the devices and the Network Server.

4. ChirpStack Join Server
***
* The join server handles the activation process for new devices joining the LoRaWAN network.
* It manages the cryptographic keys and security parameters necessary for device authentication.

### ChirpStack Gateway Configuration
***
* ChirpStack provides a way to configure and manage the settings for the connected gateways.
* It ensures a proper communication between the gateways and the network server.

#### In simple terms:
* Network Server is like the coordinator, making sure messages get to where they need to go.
* Application Server is the data hub, where you look at and make decisions based on the information received.
* Gateway Bridge is the translator, helping devices talk to the Network Server.


### REST API 
***

* With the introduction of ChirpStack v4, the REST API interface is no longer included. 
* Historically it was included to serve the web-interface, as at that time, gRPC could not be used within the browser. 
* The included REST interface internally translated REST requests into gRPC and back.

<br>

* The ChirpStack gRPC to REST API proxy is like a bridge that lets you use ChirpStack (v4) in two different ways: gRPC and REST API.
* The gRPC way is usually suggested, but sometimes people find it easier to use the REST API.
* This can be especially helpful when moving from ChirpStack v3 to v4 because, in the past, this proxy used to be a part of the ChirpStack Application Server."

#### Usage
***

1. Install package

```
sudo apt install chirpstack-rest-api
```

2. Configuration

* Environment variables can be used to configure the ChirpStack REST API proxy. You will find this configuration in /etc/chirpstack-rest-api/environment.

* (Re)start and stop

```
sudo systemctl [restart|start|stop] chirpstack-rest-api
```































