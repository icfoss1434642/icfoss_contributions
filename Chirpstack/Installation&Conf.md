# Initial Setup and Configuration
***

## System Requirements
***

* Before you begin, make sure your computer or server meets certain requirements.
* This could include having a specific operating system (like Linux), enough memory, and other software prerequisites.

# Installation
***

* Install Mosquitto, Redis, PostgreSQL

```
sudo apt install mosquitto mosquitto-clients redis-server redis-tools postgresql
```
* Switch to the PostgreSQL command-line tool

```
sudo -u postgres psql
```
* Create a role for authentication

```
create role chirpstack with login password 'chirpstack';
```

* Create a database and set chirpstack as the owner

```
create database chirpstack with owner chirpstack;
```

* Switch to the chirpstack database

```
\c chirpstack
```

* Create the pg_trgm extension

```
create extension pg_trgm;
```

* Exit the PostgreSQL command-line tool

```
\q
```

* Install required packages for ChirpStack Gateway Bridge

```
sudo apt install apt-transport-https dirmngr
```

* Add the ChirpStack repository key

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1CE2AFD36DBCCA00
```

* Add the ChirpStack repository to the sources list

```
sudo echo "deb https://artifacts.chirpstack.io/packages/4.x/deb stable main" | sudo tee /etc/apt/sources.list.d/chirpstack.list
```

* Update package information

```
sudo apt Update
```

* Install ChirpStack Gateway Bridge

```
sudo apt install chirpstack-gateway-bridge
```


## Configuration
***

* Once installed, ChirpStack needs to know some basic things to work correctly. 
* Configure things like how devices connect, where data is stored, and how security is handled.


* Configure the ChirpStack Gateway Bridge by updating the MQTT integration section in the configuration file located at /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml

```
Example for EU868:
[integration.mqtt]
event_topic_template="eu868/gateway/{{ .GatewayID }}/event/{{ .EventType }}"
command_topic_template="eu868/gateway/{{ .GatewayID }}/command/#"
```
 Here EU868 is the frequency plan of Europe. Change the plan according to your country:
 You can refer frequency plans of all the countries from the link mentioned below:
 "https://www.thethingsnetwork.org/docs/lorawan/frequencies-by-country/"


* Start ChirpStack Gateway Bridge

```
sudo systemctl start chirpstack-gateway-bridge
```

* Start ChirpStack Gateway Bridge on boot

```
sudo systemctl enable chirpstack-gateway-bridge
```

* Install ChirpStack

```
sudo apt install chirpstack
```

* Configuration files for ChirpStack are located at /etc/chirpstack.

* Start ChirpStack

```
sudo systemctl start chirpstack
```

* Start ChirpStack on boot

```
sudo systemctl enable chirpstack
```

* Check ChirpStack logs if any error occurs while running the server.

```
sudo journalctl -f -n 100 -u chirpstack
```

* Access ChirpStack web interface by going to localhost:8080 in your browser
* The default login is admin as user, and the password is also admin.

### REST API 
***

* With the introduction of ChirpStack v4, the REST API interface is no longer included. 
* Historically it was included to serve the web-interface, as at that time, gRPC could not be used within the browser. 
* The included REST interface internally translated REST requests into gRPC and back.

<br>

* The ChirpStack gRPC to REST API proxy is like a bridge that lets you use ChirpStack (v4) in two different ways: gRPC and REST API.
* The gRPC way is usually suggested, but sometimes people find it easier to use the REST API.
* This can be especially helpful when moving from ChirpStack v3 to v4 because, in the past, this proxy used to be a part of the ChirpStack Application Server."

#### Usage
***

1. Install package

```
sudo apt install chirpstack-rest-api
```

2. Configuration

* Environment variables can be used to configure the ChirpStack REST API proxy. You will find this configuration in /etc/chirpstack-rest-api/environment.

* (Re)start and stop

```
sudo systemctl [restart|start|stop] chirpstack-rest-api
```
