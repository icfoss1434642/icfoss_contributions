# Overview of ChirpStack Components
***

1. ChirpStack Network Server
***
* Purpose:
    * Acts as a traffic coordinator for LoRaWAN devices.
    * Manages communication between the IoT devices (sensors, trackers) and the application servers.
    * Ensures messages are properly delivered to the intended destination.

* Functionality:
    * Listens to messages from LoRaWAN devices.
    * Decodes and validates messages.
    * Routes messages to the appropriate destination within the network.

2. ChirpStack Application Server 
***
* Purpose:
    * Handles the data received from LoRaWAN devices.
    * Provides a dashboard for monitoring and analyzing device data.
    * Executes rules and actions based on received data.
* Functionality:
    * Collects and stores data from devices.
    * Allows users to set up rules for data processing.
    * Interfaces with external systems or applications for further actions.

3. ChirpStack Gateway Bridge
***
* Purpose:
    * Acts as a bridge between LoRaWAN devices and the ChirpStack Network Server.
    * Translates and forwards messages from devices to the Network Server.
* Functionality:
    * Listens to messages from LoRaWAN gateways.
    * Decodes and forwards messages to the ChirpStack Network Server.
    * Manages the communication protocol between the devices and the Network Server.

4. ChirpStack Join Server
***
* The join server handles the activation process for new devices joining the LoRaWAN network.
* It manages the cryptographic keys and security parameters necessary for device authentication.

### ChirpStack Gateway Configuration
***
* ChirpStack provides a way to configure and manage the settings for the connected gateways.
* It ensures a proper communication between the gateways and the network server.

#### In simple terms:
* Network Server is like the coordinator, making sure messages get to where they need to go.
* Application Server is the data hub, where you look at and make decisions based on the information received.
* Gateway Bridge is the translator, helping devices talk to the Network Server.
